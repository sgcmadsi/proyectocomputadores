<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro - Proveedores</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">
        <?php require_once("snippets/header.php"); ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Proveedores</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/proveedor_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Proveedor se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Proveedor no ha sido Registrado.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                <div class="clearfix">
                                    <label>Nombre <em>*</em><small>Nombres del Proveedor</small></label><input type="text" name="Nombre" id="Nombre" required="Nombre" maxlength="70" />
                                </div>
                                <div class="clearfix">
                                    <label>Nit o C.C <em>*</em><small>Nit o C.C del Proveedor</small></label><input type="text" name="Nit" id="Nit" required="Nit" maxlength="50" />
                                </div>
                                <div class="clearfix">
                                    <label>Direcci&oacute;n <em>*</em><small>Direcci&oacute;n del Proveedor</small></label><input type="text" name="Direccion" id="Direccion" required="Direccion" />
                                </div>
                                <div class="clearfix">
                                    <label>Telefono <em>*</em><small>Telefono del Proveedor</small></label><input type="text" name="Telefono" id="Telefono" required="Telefono" />
                                </div>
                                <div class="clearfix">
                                    <label>Descripcion <em>*</em><small>Descripcion del Proveedor</small></label><textarea type="text" name="Descripcion" id="Descripcion" required="Descripcion" maxlength="300"></textarea>
                                </div>
                                
                                <div class="clearfix">
                                    <label>Estado <em>*</em><small>Estado del Proveedor</small></label>
                                    <select id="Estado" name="Estado">
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                    </select>
                                </div>
                                <div class="clearfix">
                                    <label>Usuario <em>*</em><small>Ingrese su Usuario</small></label><input type="text" name="Usuario" id="Usuario" required="Usuario" />
                                </div>
                                <div class="clearfix">
                                    <label>Password <em>*</em><small>Ingrese su Password</small></label><input type="password" name="Contrasena" id="Contrasena" required="Contrasena" maxlength="20" />
                                </div>
                                
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
