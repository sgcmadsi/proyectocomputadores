 <!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro - Pedidos</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

                <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                       <li><a href="addpedido.php" title="Nuevo Cliente"><img src="images/woofunction-icons/user_32.png" /><span>Nuevo</span></a></li>
                       <li><a href="verpedido.php" title="Buscar Clientes"><img src="images/woofunction-icons/search_button_32.png" /><span>Busqueda</span></a></li>
                                              
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Cientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Pedidos</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/pedido_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Pedido se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Pedido no se ha registrado correctamente.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                
                                <div class="clearfix">
                                    <label>Fecha de Recibido  <em>*</em><small>Fecha de Recibido del Pedido</small></label><input type="date" name="FechaRecibido" id="FechaRecibido" required="FechaRecibido" />
                                </div>
                                <div class="clearfix">
                                    <label>Fecha de Entrega <em>*</em><small>Fecha de Entrega del Pedido</small></label><input type="date" name="FechaEntrega" id="FechaEntrega" required="FechaEntrega" />
                                </div>
                                <div class="clearfix">
                                    <label>Forma de Pago <em>*</em><small>Forma de Pago</small></label>
                                    <select id="FormaPago" name="FormaPago">
                                    <option value="Efectivo">Efectivo</option>
                                    <option value="Targeta de Credito">Targeta de Credito</option>
                                    </select>
                                </div>

                                
                                <div class="clearfix">
                                    <label>Estado <em>*</em><small>Estado del Pedido</small></label>
                                    <select id="Estado" name="Estado">
                                    <option value="Pago">Pago</option>
                                    <option value="Pendiente">Pendiente</option>
                                    </select>
                                </div>

                                <div class="clearfix">
                                    <label>Solicitante <em>*</em><small>Solicitante del Pedido</small></label><input type="text" name="Solicitante" id="Solicitante" required="Solicitante" maxlength="50" />
                                </div>

                                
                                <div class="clearfix">
                                    <label>Proveedor <em>*</em><small>Proveedor del Pedido</small></label><input type="text" name="IdProveedor" id="IdProveedor" required="IdProveedor" />
                                </div>
                                
                                                             
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
