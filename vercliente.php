<?php require_once "controladores/cliente_controller.php"; 
?>
<!DOCTYPE html> 
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ver - Clientes</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

        <?php require_once("snippets/header1.php"); ?>
        <section>
            <div class="container_8 clearfix">   
            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Cliente se ha Registrado Correctamente</p>
                                </div>
                                <?php } else { ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Cliente no se ha creado correctamente.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>             
                            
                <!-- Main Section -->
                <section class="main-section grid_8">


                    <!-- Tables Section -->
                    <div class="main-content">
                        <header>
                            <input type="text" class="search fr" placeholder="Search..."/>
                            <h2><b>Busqueda de Clientes</b></h2>
                        </header>
                        <section class="with-table">  
                          
                        <table class="datatable tablesort selectable paginate full">
                                <thead>
                                    <tr>
                                        <th style="width: 50px">Cod</th>
                                        <th style="width: 110px">Tipo Documento</th>
                                        <th>Documento</th>
                                        <th>Nombres</th>
                                        <th>Apellidos</th>
                                        <th>Direccion</th>
                                        <th>Telefono</th>
                                        <th style="width: 80px">Estado</th>
                                        <th style="width: 80px">Acciones</th>
                                    </tr>
                                </thead>
                               <tbody>
                                   <?php echo usuarios_controller::vercliente(); ?> 
                                </tbody>

                            </table>                                        
                        </section>
                    </div>
                    <!-- End Tables Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php");?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
