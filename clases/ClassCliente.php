<?php
require_once('db_abstract_class.php');

class Cliente extends db_abstract_class{
    
    private $IdCliente;
    private $TipoDocumento;
    private $Documento;
    private $Nombres;
    private $Apellidos;
    private $Direccion;
    private $Telefono;
    private $Estado;
    private $Usuario;
    private $Contrasena;

    /* METODOS GET Y SET*/
    public function getIdCliente(){
        return $this->IdCliente;
    }
    private function setIdCliente($IdCliente){
        $this->IdCliente = $IdCliente;
        return $this;
    }



    public function getTipoDocumento(){
        return $this->TipoDocumento;
    }
    private function setTipoDocumento ($TipoDocumento){
        $this->TipoDocumento = $TipoDocumento;
        return $this;
    }



    public function getDocumento(){
        return $this->Documento;
    }
    private function setDocumento ($Documento){
        $this->Documento = $Documento;
        return $this;
    }



    public function getNombres(){
        return $this->Nombres;
    }
    private function setNombres ($Nombres){
        $this->Nombres = $Nombres;
        return $this;
    }



    public function getApellidos(){
        return $this->Apellidos;
    }
    private function setApellidos($Apellidos){
        $this->Apellidos = $Apellidos;
        return $this;
    }




    
    public function getDireccion(){
        return $this->Direccion;
    }
    private function setDireccion($Direccion){
        $this->Direccion = $Direccion;
        return $this;
    }




    public function getTelefono(){
        return $this->Telefono;
    }
    private function setTelefono($Telefono){
        $this->Telefono = $Telefono;
        return $this;
    }



    public function getEstado(){
        return $this->Estado;
    }
    private function setEstado($Estado){
        $this->Estado = $Estado;
        return $this;
    }



    public function getUsuario(){
        return $this->Usuario;
    }
    private function setUsuario($Usuario){
        $this->Usuario = $Usuario;
        return $this;
    }




    public function getContrasena(){
        return $this->Contrasena;
    } 
    private function setContrasena($Contrasena){
        $this->Contrasena = $Contrasena;
        return $this;
    }


    function __destruct(){
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            $this->TipoDocumento = "";
            $this->Documento = "";
            $this->Nombres = "";
            $this->Apellidos = "";
            $this->Direccion = "";
            $this->Telefono = "";
            $this->Estado = "";
            $this->Usuario = "";
            $this->Contrasena = "";
        }
    }

     public function insertar(){
        $arrCliente = (array) $this;
        $this->insertRow("INSERT INTO Cliente
            VALUES ('?', ?, ?, ?, ?, ?, ?, ?, ?, ?)", array( 
                $this->TipoDocumento,
                $this->Documento,
                $this->Nombres,
                $this->Apellidos,
                $this->Direccion,
                $this->Telefono,
                $this->Estado,
                $this->Usuario,
                $this->Contrasena,
            )
        );
        $this->Disconnect();
    }

    public function eliminar(){
        $arrUser = (array) $this;
        $this->updateRow("DELETE FROM Cliente  WHERE IdCliente = ?", array(
            $IdCliente
        ));
        $this->Disconnect();
    }
    


    public function editar(){
        $arrCliente = (array) $this;
        $this->updateRow("UPDATE Cliente SET TipoDocumento = ?, Documento = ?, Nombres = ?, Apellidos =?, Direccion = ?, Telefono = ?, Estado = ?, Usuario = ?, Contrasena = ? WHERE IdCliente = ?", array(
                $this->TipoDocumento,
                $this->Documento,
                $this->Nombres,
                $this->Apellidos,
                $this->Direccion,
                $this->Telefono,
                $this->Estado,
                $this->Usuario,
                $this->Contrasena,
                $this->IdCliente,
        ));
        $this->Disconnect();
    }

    public static function buscarForId($IdCliente){
        if ($IdCliente > 0){
            $cliente = new Cliente();
            $getrow = $cliente->getRow("SELECT * FROM Cliente WHERE IdCliente =$IdCliente", array($IdCliente));
            $cliente->IdCliente = $getrow['IdCliente'];
            $cliente->TipoDocumento = $getrow['TipoDocumento'];
            $cliente->Documento = $getrow['Documento'];
            $cliente->Nombres = $getrow['Nombres'];
            $cliente->Apellidos = $getrow['Apellidos'];
            $cliente->Direccion = $getrow['Direccion'];
            $cliente->Telefono = $getrow['Telefono'];
            $cliente->Estado = $getrow['Estado'];
            $cliente->Usuario = $getrow['Usuario'];
            $cliente->Contrasena = $getrow['Contrasena'];
            $cliente->Disconnect();
            return $cliente;
        }else{
            return NULL;
        }
        $this->Disconnect();
    }
    
    public static function getAll(){
        return Cliente::buscar("SELECT * FROM Cliente");
    }
    
    public static function buscar($query){
        $arrCliente = array();
        $tmp = new Cliente();
        $getrows = $tmp->getrows($query);
        
        foreach ($getrows as $valor) {
            $cliente = new Cliente();
            $cliente->IdCliente = $valor['IdCliente'];
            $cliente->TipoDocumento = $valor['TipoDocumento'];
            $cliente->Documento = $valor['Documento'];
            $cliente->Nombres =  $valor['Nombres'];
            $cliente->Apellidos = $valor['Apellidos'];
            $cliente->Direccion = $valor['Direccion'];
            $cliente->Telefono = $valor['Telefono'];
            $cliente->Estado = $valor['Estado'];
            $cliente->Usuario = $valor['Usuario'];
            $cliente->Contrasena = $valor['Contrasena'];
            array_push($arrCliente, $cliente);
        }
        $tmp->Disconnect();
        return $arrCliente;
    }


    public static function Login($Usuario,$Contrasena){
        if (!empty($Usuario) && !empty($Contrasena)){
            $cliente = new Cliente();
            $valor = $cliente->getRow("SELECT * FROM Cliente WHERE Usuario =? AND Contrasena =?", array($Usuario, $Contrasena));
            if($valor != false){
                $cliente->TipoDocumento = $valor['TipoDocumento'];
                $cliente->Documento = $valor['Documento'];
                $cliente->Nombres =  $valor['Nombres'];
                $cliente->Apellidos = $valor['Apellidos'];
                $cliente->Direccion = $valor['Direccion'];
                $cliente->Telefono = $valor['Telefono'];
                $cliente->Estado = $valor['Estado'];
                $cliente->Usuario = $valor['Usuario'];
                $cliente->Contrasena = $valor['Contrasena'];
                $cliente->Disconnect();
                return $cliente;                
            }else{
                return NULL;
            }
        }else{
            return NULL;
        }
    }
}
?>