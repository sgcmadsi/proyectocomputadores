<?php
require_once('db_abstract_class.php');

class Pedido extends db_abstract_class{
	
	private $idPedido;
    private $FechaRecibio;
    private $FechaEntrega;
    private $FormaPago;
	private $Estado;
    private $Solicitante;
	


	/* Setters and Getters*/
  public function getidPedido(){
        return $this->idProveedor;
    }
    
    private function _setidPedido($Id){
        $this->idPedido = $idPedido;
        return $this;
    }

  

      public function getFechaRecibio(){
        return $this->FechaRecibio;
    }
    
    private function _setFechaRecibio($FechaRecibio){
        $this->FechaRecibio = $FechaRecibio;
        return $this;
    }

    public function getFechaEntrega(){
        return $this->FechaEntrega;
    }
    
    private function _setFechaEntrega($FechaEntrega){
        $this->FechaEntrega = $FechaEntrega;
        return $this;
    }


      public function getFormaPago(){
        return $this->FormaPago;
    }
    
    private function _setFormaPago($FormaPago){
        $this->FormaPago = $FormaPago;
        return $this;
    }

      public function getEstado(){
        return $this->Estado;
    }
    
    private function _setEstado($Estado){
        $this->Solicitante = $Solicitante;
        return $this;
    }


    public function getSolicitante(){
        return $this->Solicitante;
    }
    
    private function _setSolicitante($Solicitante){
        $this->Solicitante = $Solicitante;
        return $this;
    }

    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
			
			$this->FechaRecibio = "";
			$this->FechaEntrega = "";
			$this->FormaPago = "";
			$this->Estado = "";
            $this->Solicitante = "";
            $this->idProveedor = "";
           

			
			
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Pedido
            VALUES ('NULL', FechaRecibio,FechaEntrega, FormaPago, Estado,Solicitante,idProveedor)", array( 
                $this->FechaRecibio,
                $this->FechaEntrega,
                $this->FormaPago,
                $this->Estado,
                $this->Solicitante,
                $this->idProveedor,
                
            
               
            )
        );
        $this->Disconnect();
    }


    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$pe = new Pedido();
			$getrow = $pe->getRow("SELECT * FROM Pedido WHERE idPedido =?", array($id));
			$pe->id = $getrow['idPedido'];
			$pe->Nombre= $getrow['FechaRecibio'];
			$pe->Descripcion = $getrow['FechaEntrega'];
			$pe->Tipo= $getrow['FormaPago'];
            $pe->Categoria = $getrow['Estado']; 
            $pe->Marca= $getrow['Solicitante'];
            
            $pro= NEW Proveedor();
            $pro->buscarForId($id)->getidProveedor("idProveedor");
            $pe->_setidProveedor($pro);
             
         			
			$pe->Disconnect();
			return $pe;
		}else{
			return NULL;
		}

    }
	
	 public function buscar(){

    }

}
?>