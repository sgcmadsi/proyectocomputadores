<?php
require_once('db_abstract_class.php');

class Venta extends db_abstract_class{
    
    private $IdVenta;
    private $Cantidad;
    private  $IdCliente;
    private $NumeroVenta;
    private $ModoPago;
    private $Total;


    /* METODOS GET Y SET*/
    public function getIdVenta(){
        return $this->IdVenta;
    }
    private function setIdVenta($IdVenta){
        $this->IdVenta = $IdVenta;
        return $this;
    }



    public function getCantidad(){
        return $this->Cantidad;
    }
    private function setCantidad ($Cantidad){
        $this->Cantidad = $Cantidad;
        return $this;
    }



    public function getNumeroVenta(){
        return $this->NumeroVenta;
    }
    private function setNumeroVenta ($NumeroVenta){
        $this->NumeroVenta = $NumeroVenta;
        return $this;
    }



    public function getModoPago(){
        return $this->ModoPago;
    }
    private function setModoPago($ModoPago){
        $this->ModoPago = $ModoPago;
        return $this;
    }


    
    public function getTotal(){
        return $this->Total;
    }
    private function setTotal($Total){
        $this->Total = $Total;
        return $this;
    }



    public function getIdCliente(){
        return $this->IdCliente;
    }
    private function setIdCliente ($IdCliente){
        $this->IdCliente = $IdCliente;
        return $this;
    }


    function __destruct(){
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            $this->Fecha = "";
            $this->Cantidad = "";
            $this->NumeroVenta = "";
            $this->ModoPago = "";
            $this->Total = "";
            $this->IdCliente = "";
        }
    }

     public function insertar(){
        $arrayventa = (array) $this;
        $this->insertRow("INSERT INTO Venta
            VALUES ('NULL', Fecha, Cantidad, NumeroVenta, ModoPago, Total, IdCliente)", array( 
                $this->Fecha,
                $this->Cantidad,
                $this->Nombres,
                $this->NumeroVenta,
                $this->ModoPago,
                $this->Total,
                $this->IdCliente,
            )
        );
        $this->Disconnect();
    }

    public function editar(){
        $arrayventa = (array) $this;
        $this->updateRow("UPDATE Venta SET Fecha = $Fecha, Cantidad = $Cantidad, NumeroVenta = $NumeroVenta, ModoPago = $ModoPago, Total = $Total, IdCliente = $IdCliente, WHERE IdVenta = $IdVenta", array(
                 $this->Fecha,
                $this->Cantidad,
                $this->Nombres,
                $this->NumeroVenta,
                $this->ModoPago,
                $this->Total,
                $this->IdCliente,
                $this->IdVenta,
        ));
        $this->Disconnect();
    }

    public function eliminar(){
        return $this->IdVenta;
    }


    public static function buscarForId($IdVenta){
        if ($IdVenta > 0){
            $venta = new Venta();
            $getrow = $venta->getRow("SELECT * FROM Venta WHERE IdVenta =$IdVenta", array($IdVenta));
            $venta->IdVenta = $getrow['IdVenta'];
            $venta->Cantidad = $getrow['Cantidad'];
            $venta->NumeroVenta = $getrow['NumeroVenta'];
            $venta->ModoPago = $getrow['ModoPago'];
            $venta->Total = $getrow['Total'];

            $venta = new Cliente();
            $venta->buscarForId($IdVenta) = $getIdCliente['IdCliente'];
            $cliente->setIdCliente($venta);
            $venta->Disconnect();
            return $venta;
        }else{
            return NULL;
        }
        $this->Disconnect();
    }
    
    public static function getAll(){
        return Venta::buscar("SELECT * FROM Venta");
    }
    
    public static function buscar($query){
        $arrayventa = array();
        $tmp = new Venta();
        $getrows = $arrayventa->getRows($query);
        
        foreach ($getrows as $valor) {
            $venta = new Venta();
            $venta->IdVenta = $valor['IdVenta'];
            $venta->Fecha = $valor['Fecha'];
            $venta->Cantidad = $valor['Cantidad'];
            $venta->NumeroVenta = $valor['NumeroVenta'];
            $venta->ModoPago = $valor['ModoPago'];
            $venta->Total = $valor['Total'];
            $venta->IdCliente = $valor['IdCliente'];
            array_push($arrayventa, $venta);
        }
        $tmp->Disconnect();
        return $arrayventa;
    }

}
?>