<?php
require_once('db_abstract_class.php');

class usuarios extends db_abstract_class{
	
	private $idComputador;
	private $Marca;
	private $Procesador;
	private $Color;
	private $Bateria;
	private $TipoSistema;
	private $idproducto;
	private $MemoriaRAM;
	private $Imagen;
	private $Precio;
	

	/* Setters and Getters*/
    public function getidComputador(){
        return $this->idComputador;
    }
    
    private function _setidComputador($Id){
        $this->idComputador = $Id;
        return $this;
    }

    public function getMarca(){
        return $this->Marca;
    }
    
    private function _setMarca ($Marca){
        $this->Marca = $Marca;
        return $this;
    }

    public function getProcesador(){
        return $this->Procesador;
    }
    
    private function _setProcesador ($Procesador){
        $this->Procesador = $Procesador;
        return $this;
    }

    public function getidproducto(){
        return $this->idproducto;
    }
    
    private function _setidproducto ($idproducto){
        $this->idproducto = $idproducto;
        return $this;
    }

    public function getMemoriaRAM(){
        return $this->MemoriaRAM;
    }
    
    private function _setMemoriaRAM($MemoriaRAM){
        $this->MemoriaRAM = $MemoriaRAM;
        return $this;
    }

    public function getImagen(){
        return $this->Imagen;
    }

    private function _setImagen($Imagen){
        $this->Imagen = $Imagen;
        return $this;
    }

    public function getPrecio(){
        return $this->Precio;
    }
    
    private function _setPrecio($Precio){
        $this->Precio = $Precio;
        return $this;
    }

   

    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
			$this->Marca = "";
			$this->Procesador = "";
			$this->Color = "";
			$this->Bateria = "";
			$this->TipoSistema = "";
			$this->idproducto = "";
			$this->MemoriaRAM = "";
			$this->Imagen = "";
			$this->Precio = "";
			
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Computador
            VALUES ('NULL', ?, ?, ?, ?, ?, ?, ?, ?, ?)", array( 
                $this->Marca,
                $this->Procesador,
                $this->Color,
                $this->Bateria,
                $this->TipoSistema,
                $this->idproducto,
                $this->MemoriaRAM,
                $this->Imagen,
                $this->Precio,
               
            )
        );
		$this->Disconnect();
    }

    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$comp = new computador();
			$getrow = $usr->getRow("SELECT * FROM Computador WHERE idComputador =?", array($id));
			$usr->id = $getrow['idComputador'];
			$usr->tipo_identificacion = $getrow['Marca'];
			$usr->identificacion = $getrow['Procesador'];
			$usr->nombres = $getrow['Color'];
			$usr->apellidos = $getrow['Bateria'];
			$usr->telefono = $getrow['TipoSistema'];
			$usr->direccion = $getrow['idproducto'];
			$usr->fecha_nacimiento = $getrow['MemoriaRAM'];
			$usr->saldo = $getrow['Imagen'];
			$usr->user_login = $getrow['Precio'];
			$usr->Disconnect();
			return $usr;
		}else{
			return NULL;
		}

    }
	
	 public function buscar(){

    }

}
?>