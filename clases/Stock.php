<?php
require_once('db_abstract_class.php');

class Stock extends db_abstract_class{
    
    private $idStock;
    private $Cantidad;
    private $ValorUnitario;
    private $ValorVenta;

    


    /* Setters and Getters*/
  public function getidStock(){
        return $this->idStock;
    }
    
    private function _setidStock($idStock){
        $this->idStock = $idStock;
        return $this;
    }


    public function getCantidad(){
        return $this->Cantidad;
    }
    
    private function _setCantidad($Cantidad){
        $this->Cantidad= $Cantidad;
        return $this;
    }


      public function getValorUnitario(){
        return $this->ValorUnitario;
    }
    
    private function _setValorUnitario($ValorUnitario){
        $this->ValorUnitario = $ValorUnitario;
        return $this;
    }

      public function getValorVenta(){
        return $this->ValorVenta;
    }
    
    private function _setValorVenta($ValorVenta){
        $this->ValorVenta = $ValorVenta;
        return $this;
    }



    function __destruct() {
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            
            $this->Cantidad = "";
            $this->ValorUnitario = "";
            $this->ValorVenta = "";
            $this->idPedido = "";
            $this->idSuministro= "";
           

            
            
        }
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Stock
            VALUES ('NULL',Cantidad,ValorUnitario ValorVenta,idPedido,idSuministro)", array( 
                $this->Cantidad,
                $this->ValorUnitario,
                $this->ValorVenta,
                $this->idPedido,
                $this->idSuministro,
                
            
               
            )
        );
        $this->Disconnect();
    }


    public function editar(){
        
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
        if ($id > 0){
            $pe = new Venta();
            $getrow = $pe->getRow("SELECT * FROM Stock WHERE idStock =?", array($id));
            $pe->id = $getrow['idStock'];
            $pe->Cantidad = $getrow['Cantidad'];
            $pe->ValorUnitario= $getrow['ValorUnitario'];
            $pe->ValorVenta = $getrow['ValorVenta']; 
            
            
            $pro= NEW Pedido();
            $pro->buscarForId($id)->getidPedido("idPedido");
            $pe->_setidPedido($pro);
             



            $pro= NEW Suministro();
            $pro->buscarForId($id)->getidSuministro("idSuministro");
            $pe->_setidSuministro($pro);
             
                    
            $pe->Disconnect();
            return $pe;
        }else{
            return NULL;
        }

    }
    
     public function buscar(){

    }

}
?>