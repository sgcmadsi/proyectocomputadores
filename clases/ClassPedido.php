<?php
require_once('db_abstract_class.php');

class Pedido extends db_abstract_class{
    
    private $IdPedido;
    private $FechaRecibido;
    private $FechaEntrega;
    private $FormaPago;
    private $Estado;
    private $Solicitante;
    private  $IdProveedor;
   

    /* METODOS GET Y SET*/
    public function getIdPedido(){
        return $this->IdPedido;
    }
    private function setIPedido($IdPedido){
        $this->IdPedido = $IdPedido
        ;
        return $this;
    }



    public function getFechaRecibido(){
        return $this->FechaRecibido;
    }
    private function setFechaRecibido($FechaRecibido){
        $this->FechaRecibido = $FechaRecibido;
        return $this;
    }



    public function getFechaEntrega(){
        return $this->FechaEntrega;
    }
    private function setFechaEntrega($FechaEntrega){
        $this->FechaEntrega = $FechaEntrega;
        return $this;
    }



    public function getFormaPago(){
        return $this->FormaPago;
    }
    private function setFormaPago ($FormaPago){
        $this->FormaPago = $FormaPago;
        return $this;
    }



    public function getEstado(){
        return $this->Estado;
    }
    private function setEstado($Estado){
        $this->Estado = $Estado;
        return $this;
    }


    
    public function getSolicitante(){
        return $this->Solicitante;
    }
    private function setSolicitante($Solicitante){
        $this->Solicitante = $Solicitante;
        return $this;
    }




    public function getIdProveedor(){
        return $this->Proveedor;
    }
    private function setIdProveedor($Proveedor){
        $this->Proveedor = $Proveedor;
        return $this;
    }



    function __destruct(){
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            $this->FechaRecibido = "";
            $this->FechaEntrega = "";
            $this->FormaPago = "";
            $this->Estado = "";
            $this->Solicitante = "";
            $this->IdProveedor = "";
           
        }
    }

     public function insertar(){
        $arrPedido = (array) $this;
        $this->insertRow("INSERT INTO Pedido
            VALUES ('NULL', FechaRecibido, FechaEntrega, FormaPago, Estado, Solicitante, IdProveedor)", array( 
                $this->FechaRecibido,
                $this->FechaEntrega,
                $this->FormaPago,
                $this->Estado,
                $this->Solicitante,
                $this->IdProveedor,
            )
        );
        $this->Disconnect();
    }

    public function editar(){
        $arrPedido = (array) $this;
        $this->updateRow("UPDATE Pedido SET FechaRecibido = $FechaRecibido, FechaEntrega = $FechaEntrega, FormaPago = $FormaPago, Estado = $Estado, Solicitante = $Solicitante, IdProveedor = $IdProveedor   WHERE IdPedido = $IdPedido", array(
                $this->FechaRecibido,
                $this->FechaEntrega,
                $this->FormaPago,
                $this->Estado,
                $this->Solicitante,
                $this->IdProveedor,
                $this->IdPedido, 
        ));
        $this->Disconnect();
    }

    public function eliminar(){
        return $this->IdPedido;
    }


    public static function buscarForId($IdPedido){
        if ($IdPedido > 0){
            $pedido = new Suministro();
            $getrow = $pedido->getRow("SELECT * FROM Pedido WHERE IdPedido = $IdPedido", array($IdPedido));
            $pedido->IdPedido = $getrow['IdPedido'];
            $pedido->FechaRecibido = $getrow['FechaRecibido'];
            $pedido->FechaEntrega = $getrow['FechaEntrega'];
            $pedido->FormaPago = $getrow['FormaPago'];
            $pedido->Estado = $getrow['Estado'];
            $pedido->Solicitante = $getrow['Solicitante'];

            $pedido = new Proveedor();
            $pedido->buscarForId($IdPedido) = $getIdProveedor ['IdPedido'];
            $prov->setIdProveedor($pedido);

            $pedido->Disconnect();
            return $pedido;
        }else{
            return NULL;
        }
        $this->Disconnect();
    }
    
    public static function getAll(){
        return Proveedor::buscar("SELECT * FROM Pedido");
    }
    
    public static function buscar($query){
        $arrPedido = array();
        $tmp = new Pedido();
        $getrows = $arrPedido->getRows($query);
        
        foreach ($getrows as $valor) {
            $pedido = new Pedido();
            $pedido->IdPedido = $valor['IdPedido'];
            $pedido->FechaRecibido = $valor['FechaRecibido'];
            $pedido->FechaEntrega = $valor['FechaEntrega'];
            $pedido->FormaPago = $valor['FormaPago'];
            $pedido->Estado = $valor['Estado'];
            $pedido->Solicitante = $valor['Solicitante'];
            $pedido->IdProveedor = $valor['IdProveedor'];
            array_push($arrPedido, $pedido);
        }
        $tmp->Disconnect();
        return $arrPedido;
    }

}
?>