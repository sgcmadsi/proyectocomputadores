<?php
require_once('db_abstract_class.php');

class Cliente extends db_abstract_class{
	
	private $idCliente;
	private $TipoDocumento = "";
	private $Documento;
	private $Nombres;
	private $Apellidos;
    private $Direccion;
	private $Telefono;
    private $Estado = "";
    private $Usuario;
    private $Contrasena;
	
	

	/* Setters and Getters*/
    public function getidCliente(){
        return $this->idCliente;
    }
    
    private function _setidCliente($Id){
        $this->idCliente = $Id;
        return $this;
    }

    public function getTipoDocumento(){
        return $this->Usuario;
    }
    
    private function _setTipoDocumento ($TipoDocumento){
        $this->TipoDocumento = $TipoDocumento;
        return $this;
    }

public function getDocumento(){
        return $this->Usuario;
    }
    
    private function _setDocumento ($Documento){
        $this->Documento = $Documento;
        return $this;
    }

    public function getNombres(){
        return $this->Nombres;
    }
    
    private function _setNombres ($Nombres){
        $this->Nombres = $Nombres;
        return $this;
    }

    public function getApellidos(){
        return $this->Apellidos;
    }
    
    private function _setApellidos ($Apellidos){
        $this->Apellidos = $Apellidos;
        return $this;
    }

 public function getDireccion(){
        return $this->Direccion;
    }

 
    private function _setDireccion($Direccion){
        $this->Direccion = $Direccion;
        return $this;
    }
    
    public function getTelefono(){
        return $this->Telefono;
    }
    
   
 
    private function _setTelefono($Telefono){
        $this->Telefono = $Telefono;
        return $this;
    }        
    
    public function getEstado(){
        return $this->Estado;
    }
    
   
 
    private function _setEstado($Estado){
        $this->Estado = $Estado;
        return $this;
    }   
   

public function getUsuario(){
        return $this->Usuario;
    }
    
   
 
    private function _setUsuario($Usuario){
        $this->Usuario = $Usuario;
        return $this;
    }   


    public function getContrasena(){
        return $this->Estado;
    }
    
   
 
    private function _setContrasena($Contrasena){
        $this->Contrasena = $Contrasena;
        return $this;
    }   





    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
			$this->TipoDocumento = "";
			$this->Documento = "";
			$this->Nombres = "";
			$this->Apellidos = "";
            $this->Direccion = "";
            $this->Telefono = "";
			$this->Estado = "";
            $this->Usuario = "";
            $this->Contrasena = "";
			
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Cliente
            VALUES ('NULL', TipoDocumento, Documento, Nombres, Apellidos,Direccion, Telefono,Estado,Usuario,Contrasena)", array( 
                $this->TipoDocumento,
                $this->Documento,
                $this->Nombres,
                $this->Apellidos,
                $this->Direccion,
                $this->Telefono,
                $this->Estado,
                $this->Usuario,
                $this->Contrasena,
            
               
            )
        );
		$this->Disconnect();
    }

    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$comp = new Cliente();
			$getrow = $usr->getRow("SELECT * FROM Cliente WHERE idCliente =?", array($id));
			$usr->id = $getrow['idCliente'];
			$usr->TipoDocumento = $getrow['TipoDocumento'];
			$usr->Documento = $getrow['Documento'];
			$usr->Nombres = $getrow['Nombres'];
			$usr->Apellidos = $getrow['Apellidos'];
            $usr->Direccion = $getrow['Direccion'];
			$usr->Telefono = $getrow['Telefono'];
			$usr->Estado = $getrow['Estado'];
            
			
			$usr->Disconnect();
			return $usr;
		}else{
			return NULL;
		}

    }
	
	 public function buscar(){

    }

}
?>