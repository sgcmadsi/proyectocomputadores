<?php
require_once('db_abstract_class.php');

class Suministro extends db_abstract_class{
    
    private $IdSuministro;
    private $Descripcion;
    private $Tipo;
    private $Categoria;
    private $Marca;
    private $Referencia;
    

    /* METODOS GET Y SET*/
    public function getIdSuministro(){
        return $this->IdSuministro;
    }
    private function setIdProveedor($IdSuministro){
        $this->IdSuministro = $IdSuministro;
        return $this;
    }



    public function getNombre(){
        return $this->Nombre;
    }
    private function setNombre ($Nombre){
        $this->Nombre = $Nombre;
        return $this;
    }


    public function getDescripcion(){
        return $this->Descripcion;
    }
    private function setDescripcion ($Descripcion){
        $this->Descripcion = $Descripcion;
        return $this;
    }



    public function getTipo(){
        return $this->Tipo;
    }
    private function setTipo ($Tipo){
        $this->Tipo = $Tipo;
        return $this;
    }



    public function getCategoria(){
        return $this->Categoria;
    }
    private function setCategoria ($Categoria){
        $this->Categoria = $Categoria;
        return $this;
    }



    public function getMarca(){
        return $this->Marca;
    }
    private function setMarca($Marca){
        $this->Marca = $Marca;
        return $this;
    }


    
    public function getReferencia(){
        return $this->Referencia;
    }
    private function setReferencia($Referencia){
        $this->Referencia = $Referencia;
        return $this;
    }



    function __destruct(){
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            $this->Nombre = "";
            $this->Descripcion = "";
            $this->Tipo = "";
            $this->Categoria = "";
            $this->Marca = "";
            $this->Referencia = "";
        }
    }

     public function insertar(){
        $arrSuministro = (array) $this;
        $this->insertRow("INSERT INTO Suministro
            VALUES ('?', ?, ?, ?, ?, ?, ?)", array( 
                $this->Nombre, 
                $this->Descripcion,
                $this->Tipo,
                $this->Categoria,
                $this->Marca,
                $this->Referencia,
            )
        );
        $this->Disconnect();
    }

    public function editar(){
        $arrSuministro = (array) $this;
        $this->updateRow("UPDATE Suministro SET Nombre = $Nombre, Descripcion = $Descripcion, Tipo = $Tipo, Categoria = $Categoria, Marca = $Marca, Referencia = $Referencia WHERE IdSuministro = $IdSuministro", array(
                $this->Nombre,
                $this->Descripcion,
                $this->Tipo,
                $this->Categoria,
                $this->Marca,
                $this->Referencia,
                $this->IdSuministro, 
        ));
        $this->Disconnect();
    }

    public function eliminar(){
        return $this->IdSuministro;
    }


    public static function buscarForId($IdSuministro){
        if ($IdSuministro > 0){
            $suministro = new Suministro();
            $getrow = $suministro->getRow("SELECT * FROM Suministro WHERE IdSuministro =$IdSuministro", array($IdSuministro));
            $suministro->IdSuministro = $getrow['IdSuministro'];
            $suministro->Nombre = $getrow['Nombre'];
            $suministro->Descripcion = $getrow['Descripcion'];
            $suministro->Tipo = $getrow['Tipo'];
            $suministro->Categoria = $getrow['Categoria'];
            $suministro->Marca = $getrow['Marca'];
            $suministro->Referencia = $getrow['Referencia'];
            $suministro->Disconnect();
            return $suministro;
        }else{
            return NULL;
        }
        $this->Disconnect();
    }
    
    public static function getAll(){
        return Suministro::buscar("SELECT * FROM Suministro");
    }
    
    public static function buscar($query){
        $arrSuministro = array();
        $tmp = new Suministro();
        $getrows = $tmp->getRows($query);
        
        foreach ($getrows as $valor) {
            $suministro = new Suministro();
            $suministro->IdSuministro = $valor['IdSuministro'];
            $suministro->Nombre = $valor['Nombre'];
            $suministro->Descripcion = $valor['Descripcion'];
            $suministro->Tipo = $valor['Tipo'];
            $suministro->Categoria = $valor['Categoria'];
            $suministro->Marca = $valor['Marca'];
            $suministro->Referencia = $valor['Referencia'];
            array_push($arrSuministro, $suministro);
        }
        $tmp->Disconnect();
        return $arrSuministro;
    }

}
?>