<?php
require_once('db_abstract_class.php');

class Suministro extends db_abstract_class{
	
	private $id;
	private $Nombre;
	private $Descripcion;
	private $Tipo;
	private $Categoria;
    private $Marca;
private $Referencia;

	/* Setters and Getters*/
    public function getId(){
        return $this->id;
    }
    
    private function _setId ($Id){
        $this->id = $Id;
        return $this;
    }

    public function getNombre(){
        return $this->Nombre;
    }
    
    private function _setNombre ($Nombre){
        $this->Nombre = $Nombre;
        return $this;
    }

    public function getDescripcion(){
        return $this->Descripcion;
    }
    
    private function _setDescripcion ($Descripcion){
        $this->Descripcion= $Descripcion;
        return $this;
    }

    public function getTipo(){
        return $this->Tipo;
    }
    
    private function _setTipo ($Tipo){
        $this->Tipo = $Tipo;
        return $this;
    }

    public function getCategoria(){
        return $this->Categoria;
    }
    
    private function _setCategoria($Categoria){
        $this->Categoria = $Categoria;
        return $this;
    }

    public function getMarca(){
        return $this->Marca;
    }

    private function _setReferencia($Referencia){
        $this->Referencia = $Referencia;
        return $this;
    }

    


    function __destruct() {
        $this->Disconnect();
    }

	public function __construct($user_data=array()){
        parent::__construct();
		if(count($user_data)>1){
			foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
			}
		}else {
			$this->Nombre = "";
             $this->Descripcion= "";
			$this->Tipo= "";
           $this->Categoria = "";
			$this->Marca= "";
			$this->Referencia = "";
		}
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Suministro
            VALUES ('NULL',Nombre,Descripcion,Tipo,Categoria,Marca,Referencia)", array( 
                
$this->Nombre,
             $this->Descripcion,
            $this->Tipo,
           $this->Categoria,
            $this->Marca,
            $this->Referencia,
        



            )
        );
		$this->Disconnect();
    }

    public function editar(){
		
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
		if ($id > 0){
			$usr = new Suministro();
			$getrow = $usr->getRow("SELECT * FROM Suministro WHERE idSuministro =?", array($id));
			$usr->id = $getrow['idSuministro'];
			$usr->Nombre = $getrow['Nombre'];
			$usr->Descripcion = $getrow['Descripcion'];
			$usr->Tipo = $getrow['Tipo'];
			$usr->Categoria = $getrow['Categoria'];
			$usr->Marca = $getrow['Marca'];
$usr->Referencia = $getrow['Referencia'];
			$usr->Disconnect();
			return $usr;
		}else{
			return NULL;
		}

    }
	
	 public function buscar(){

    }

}
?>