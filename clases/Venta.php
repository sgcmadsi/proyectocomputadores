<?php
require_once('db_abstract_class.php');

class Venta extends db_abstract_class{
    
    private $idVenta;
    private $Fecha;
    private $Cantidad;
    private $ModoPago;
    private $NumeroVenta;
    private $Total;
    


    /* Setters and Getters*/
  public function getidVenta(){
        return $this->idVenta;
    }
    
    private function _setidVenta($idVenta){
        $this->idVenta = $idVenta;
        return $this;
    }

  

      public function getFecha(){
        return $this->Fecha;
    }
    
    private function _setFecha($Fecha){
        $this->Fecha = $Fecha;
        return $this;
    }

    public function getCantidad(){
        return $this->Cantidad;
    }
    
    private function _setCantidad($Cantidad){
        $this->Cantidad= $Cantidad;
        return $this;
    }


      public function getModoPago(){
        return $this->ModoPago;
    }
    
    private function _setModoPago($ModoPago){
        $this->ModoPago = $ModoPago;
        return $this;
    }

      public function getNumeroVenta(){
        return $this->NumeroVenta;
    }
    
    private function _setNumeroVenta($NumeroVenta){
        $this->NumeroVenta = $NumeroVenta;
        return $this;
    }


    public function getTotal(){
        return $this->Total;
    }
    
    private function _setTotal($Total){
        $this->Total = $Total;
        return $this;
    }

    function __destruct() {
        $this->Disconnect();
    }

    public function __construct($user_data=array()){
        parent::__construct();
        if(count($user_data)>1){
            foreach ($user_data as $campo=>$valor){
                $this->$campo = $valor;
            }
        }else {
            
            $this->Fecha = "";
            $this->Cantidad = "";
            $this->ModoPago = "";
            $this->NumeroVenta = "";
            $this->Total = "";
            $this->idCliente= "";
           

            
            
        }
    }

    public function insertar(){
        $arrUser = (array) $this;
        $this->insertRow("INSERT INTO Venta
            VALUES ('NULL', Fecha,Cantidad, ModoPago, NumeroVenta,Total,idCliente)", array( 
                $this->Fecha,
                $this->Cantidad,
                $this->ModoPago,
                $this->NumeroVenta,
                $this->Total,
                $this->idCliente,
                
            
               
            )
        );
        $this->Disconnect();
    }


    public function editar(){
        
        return $this->user_login;
    }

    public function eliminar(){
        return $this->user_login;
    }

    public static function buscarForId($id){
        if ($id > 0){
            $pe = new Venta();
            $getrow = $pe->getRow("SELECT * FROM Venta WHERE idVenta =?", array($id));
            $pe->id = $getrow['idVenta'];
            $pe->Fecha= $getrow['Fecha'];
            $pe->Cantidad = $getrow['Cantidad'];
            $pe->ModoPago= $getrow['ModoPago'];
            $pe->NumeroVenta = $getrow['NumeroVenta']; 
            $pe->Total= $getrow['Total'];
            
            $pro= NEW Cliente();
            $pro->buscarForId($id)->getidCliente("idCliente");
            $pe->_setidCliente($pro);
             
                    
            $pe->Disconnect();
            return $pe;
        }else{
            return NULL;
        }

    }
    
     public function buscar(){

    }

}
?>