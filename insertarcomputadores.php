<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ejemplo - PHP POO</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

        <?php require_once("snippets/header.php"); ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registrar Clientes</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/Computadores_controller.php?action=crear" method="POST">

                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Computador se ha creado correctamente.</p>
                                </div>
                                <?php } else { ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Computador no se ha creado correctamente.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                
                                <div class="clearfix">
                                    <label>Marca <em>*</em><small>ingrese la marca del computador</small></label><input type="text" name="Marca" id="Marca" required="Marca" maxlength="12" />
                                </div>
                                <div class="clearfix">
                                    <label>Procesador <em>*</em><small>ingrese que procesador es</small></label><input type="text" name="Procesador" id="Procesador" required="Contrasena" />
                                </div>
                                <div class="clearfix">
                                    <label>Color <em>*</em><small>Ingrese el color</small></label><input type="text" name="Color" id="Color" required="required" />
                                </div>
                                <div class="clearfix">
                                    <label>Bateria <em>*</em><small>ingrese que tipo de bateria es</small></label><input type="text" name="Bateria" id="Bateria" required="required" maxlength="12" />
                                </div>
                                <div class="clearfix">
                                    <label>TipoSistema <em>*</em><small>TipoSistema</small></label><input type="text" name="TipoSistema" id="TipoSistema" required="required" maxlength="30" />
                                </div>

                                    <label>idProducto <em>*</em><small>idProducto</small> </label><input  type="text" name="idProducto" id="idProducto" required="required" maxlength="30" />

                                <div class="clearfix">
                                    <label>MemoriaRAM<em>*</em><small>MemoriaRAM</small></label><input type="text" name="MemoriaRAM" id="MemoriaRAM" />
                                </div>
                                <div class="clearfix">
                                    <label>Imagen<em>*</em><small>Imagen</small></label><input type="text" name="Imagen" id="Imagen" required="required" maxlength="12" />
                                </div>
                               
                               <div class="clearfix">
                                    <label>Precio<em>*</em><small>Precio</small></label><input type="text" name="Precio" id="Precio" required="required" maxlength="12" />
                                </div>
                               
                                
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
