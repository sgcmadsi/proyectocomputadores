<?php
require_once(__DIR__.'/../clases/ClassSuministro.php');

if(!empty($_GET['action'])){
	suministro_controller::main($_GET['action']);
}

class suministro_controller{
	
	static function main($action){
		if ($action == "crear"){
			suministro_controller::crear();
		}else if ($action == "editar"){
			suministro_controller::editar();
		}else if ($action == "buscarID"){
			suministro_controller::buscarID(1);
		}else if ($action == "versuministro") {
			suministro_controller::versuministro();
		}
	}
	
	static public function crear (){
		try {
			$arraySuministro = array();
			$arraySuministro['Nombre'] = $_POST['Nombre'];
			$arraySuministro['Descripcion'] = $_POST['Descripcion'];
			$arraySuministro['Tipo'] = $_POST['Tipo'];
			$arraySuministro['Categoria'] = $_POST['Categoria'];
			$arraySuministro['Marca'] = $_POST['Marca'];
			$arraySuministro['Referencia'] = $_POST['Referencia'];
			$suministro = new Suministro($arraySuministro);
			$suministro->insertar();
			header("Location: ../versuministro.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../versuministro.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arraySuministro = array();
			$arraySuministro['Nombre'] = $_POST['Nombre'];
			$arraySuministro['Descripcion'] = $_POST['Descripcion'];
			$arraySuministro['Tipo'] = $_POST['Tipo'];
			$arraySuministro['Categoria'] = $_POST['Categoria'];
			$arraySuministro['Marca'] = $_POST['Marca'];
			$arraySuministro['IdSuministro'] = $_GET['IdSuministro'];
			$suministro = new Suministro($arraySuministro);
			$suministro->editar();
			header("Location: ../versuministro.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../versuministro.php?respuesta=error");
		}
	}
	
	static public function buscarID ($id){
		try { 
			return Suministro::buscarForId($id);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
	public function buscarAll (){
		try {
			return Suministro::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	static public function versuministro(){
		try { 
			$suministro = new Suministro();
			$arraySuministro = $suministro::getAll();
			if (count($arraySuministro>0)) {
				foreach ($arraySuministro as $sumin) {
					echo "<tr>";
					echo "<td>".$sumin->getIdSuministro()."</td>";
					echo "<td>".$sumin->getNombre()."</td>";
					echo "<td>".$sumin->getDescripcion()."</td>";
					echo "<td>".$sumin->getTipo()."</td>";
					echo "<td>".$sumin->getCategoria()."</td>";
					echo "<td>".$sumin->getMarca()."</td>";
					echo "<td>".$sumin->getReferencia()."</td>";
					echo "<td></td>";
					echo "</tr>";

				}
			}else{
				echo "No existen Suministros Registrados";
			}

			
		} catch (Exception $e) {
			
		}
	}

	public function eliminar(){
        $conexion = mysql_connect("localhost","Victor","Victor");
        if (!$conexion) {
            die ("No se puede conectar".mysql_error());
        }
        //selecciono la bases de datos
        mysql_select_db("Computadores",$conexion);
        $IdSuministro = @$_GET['IdSuministro'];
        //query
        $borrar= mysql_query("delete from Suministro where IdSuministro= $IdSuministro");
        if ($borrar==true) {
        header("Location: ../vercliente.php ");
         
        }else{
        header("Location: ../vercliente.php ");
        }

        mysql_close($conexion);
    }	
}

?>