<?php

require_once ('../clases/ClassPedido.php');

if(!empty($_GET['action'])){
	pedido_controller::main($_GET['action']);
}

class pedido_controller{
	
	static function main($action){
		if ($action == "crear"){
			pedido_controller::crear();
		}else if ($action == "editar"){
			pedido_controller::editar();
		}else if ($action == "buscarID"){
			pedido_controller::buscarID(1);
		}
	}
	
	static public function crear(){
		try {
			$arrayPedido = array();
			$arrayPedido['FechaRecibido'] = $_POST['FechaRecibido'];
			$arrayPedido['FechaEntrega'] = $_POST['FechaEntrega'];
			$arrayPedido['FormaPago'] = $_POST['FormaPago'];
			$arrayPedido['Estado'] = $_POST['Estado'];
			$arrayPedido['Solicitante'] = $_POST['Solicitante'];
			$arrayPedido['IdProveedor'] = $_POST['IdProveedor'];
			$pedido = new Pedido ($arrayPedido);
			$pedido->insertar();
			header("Location: ../verpedido.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../addpedido.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arrayPedido = array();
			$arrayPedido['FechaRecibido'] = $_POST['FechaRecibido'];
			$arrayPedido['FechaEntrega'] = $_POST['FechaEntrega'];
			$arrayPedido['FormaPago'] = $_POST['FormaPago'];
			$arrayPedido['Estado'] = $_POST['Estado'];
			$arrayPedido['Solicitante'] = $_POST['Solicitante'];
			$arrayPedido['IdProveedor'] = $_POST['IdProveedor'];
			$arrayPedido['IdPedido'] = $_POST['IdPedido'];
			var_dump($arrayPedido);
			$pedido = new Pedido ($arrayPedido);
			$pedido->editar();
			header("Location: ../editar.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../editar.php?respuesta=error");
		}
	}
	
	static public function buscarID ($IdPedido){
		try { 
			return Pedido::buscarForId($IdProveedor);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
	public function buscarAll (){
		try {
			return Pedido::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	public function buscar ($campo, $parametro){
		try {
			return Pedido::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
}
?>