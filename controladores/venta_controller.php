<?php

require_once ('../clases/ClassVenta.php');

if(!empty($_GET['action'])){
	cliente_controller::main($_GET['action']);
}

class cliente_controller{
	
	static function main($action){
		if ($action == "crear"){
			cliente_controller::crear();
		}else if ($action == "editar"){
			cliente_controller::editar();
		}else if ($action == "buscarID"){
			cliente_controller::buscarID(1);
		}
	}
	
	static public function crear(){
		try {
			$arrayVenta = array();
			$arrayVenta['Fecha'] = $_POST['Fecha'];
			$arrayVenta['Cantidad'] = $_POST['Cantidad'];
			$arrayVenta['NumeroVenta'] = $_POST['NumeroVenta'];
			$arrayVenta['ModoPago'] = $_POST['ModoPago'];
			$arrayVenta['Total'] = $_POST['Total'];
			$arrayVenta['IdCliente'] = $_POST['IdCliente'];
			$venta = new Venta ($arrayVenta);
			$venta->insertar();
			header("Location: ../verventa.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../addventa.php?respuesta=error");
		}
	}
	
	static public function editar (){
		try {
			$arrayVenta = array();
			$arrayVenta['Fecha'] = $_POST['Fecha'];
			$arrayVenta['Cantidad'] = $_POST['Cantidad'];
			$arrayVenta['NumeroVenta'] = $_POST['NumeroVenta'];
			$arrayVenta['ModoPago'] = $_POST['ModoPago'];
			$arrayVenta['Total'] = $_POST['Total'];
			$arrayVenta['IdCliente'] = $_POST['IdCliente'];
			$arrayVenta['IdVenta'] = $_POST['IdVenta'];
			var_dump($arrayVenta);
			$usuario = new Venta ($arrayVenta);
			$usuario->editar();
			header("Location: ../editar.php?respuesta=correcto");
		} catch (Exception $e) {
			header("Location: ../editar.php?respuesta=error");
		}
	}
	
	static public function buscarID ($IdVenta){
		try { 
			return Venta::buscarForId($IdVenta);
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
	public function buscarAll (){
		try {
			return Venta::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}

	public function buscar ($campo, $parametro){
		try {
			return Venta::getAll();
		} catch (Exception $e) {
			header("Location: ../buscar.php?respuesta=error");
		}
	}
	
}
?>