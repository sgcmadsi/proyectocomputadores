<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro - Ventas</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>
    <div id="wrapper">

                <header>
            <div class="clearfix">
                <div class="clear"></div>

                <a class="chevron fr">Expand/Collapse</a>
                <nav>
                    <ul class="clearfix">
                        <li><a href="addventa.php" title="Nueva Venta"><img src="images/woofunction-icons/user_32.png" /><span>Nueva</span></a></li>
                        <li><a href="verventa.php" title="Buscar Ventas"><img src="images/woofunction-icons/search_button_32.png" /><span>Busqueda</span></a></li>
                        
                        
                        <li class="fr action">
                            <a href="documentation/index.html" class="button button-orange help" rel="#overlay"><span class="help"></span>Help</a>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Contact</a>
                            <div class="popupballoon bottom">
                                <h3>Add new contact</h3>
                                First Name<br />
                                <input type="text" /><br />
                                Last Name<br />
                                <input type="text" /><br />
                                Company<br />
                                <input type="text" />
                                <hr />
                                <button class="button button-orange">Add contact</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr action">
                            <a href="#" class="has-popupballoon button button-blue"><span class="add"></span>New Task</a>
                            <div class="popupballoon bottom">
                                <h3>Add new task</h3>
                                <input type="text" /><br /><br />
                                When it's due?<br />
                                <input type="date" /><br />
                                What category?<br />
                                <select><option>None</option></select>
                                <hr />
                                <button class="button button-orange">Add task</button>
                                <button class="button button-gray close">Cancel</button>
                            </div>
                        </li>
                        <li class="fr"><a href="#" class="arrow-down">administrator</a>
                            <ul>
                                <li><a href="#">Account</a></li>
                                <li><a href="#">Cientes</a></li>
                                <li><a href="#">Groups</a></li>
                                <li><a href="#">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Ventas</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/venta_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Venta se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Venta no ha sido creada.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                <div class="clearfix">
                                    <label>Fecha <em>*</em><small>Fecha de la Venta</small></label><input type="date" name="Fecha" id="Fecha" required="Fecha"/>
                                </div>
                                <div class="clearfix">
                                    <label>Cantidad <em>*</em><small>Cantidad a Vender</small></label><input type="text" name="Cantidad" id="Cantidad" required onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"/>
                                </div>
                                
                                <div class="clearfix">
                                    <label>Numero de Venta <em>*</em><small>Numero de Venta</small></label><input type="text" name="NumeroVenta" id="NumeroVenta" required="NumeroVenta" />
                                </div>
                                                               
                                <div class="clearfix">
                                    <label>Modo de Pago <em>*</em><small>Modo de Pago </small></label>
                                    <select id="ModoPago" name="ModoPago">
                                    <option value="Efectivo">Efectivo</option>
                                    <option value="Targeta de Credito">Targeta de Credito</option>
                                    <option value="Otro">Otro</option>
                                    </select>
                                </div>
                                

                                <div class="clearfix">
                                    <label>Total <em>*</em><small>Valor total de la Compra</small></label><input type="text" name="Total" id="Total" required="Total" />
                                </div>

                                <div class="clearfix">
                                    <label>Cliente <em>*</em><small>Cliente que realiza la compra</small></label>
                                    <input type="text" name="IdCliente" id="IdCliente" required/>
                                </div>
                                
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Registro de Usuarios</h2></header>
                            <img src=""; ></img>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
