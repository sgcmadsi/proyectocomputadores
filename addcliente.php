 <!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registro - Clientes</title>
<?php require_once("snippets/includes_files.php"); ?>
</head>
<body>

    <div id="wrapper">

        <?php require_once("snippets/header1.php"); ?>
        <section>
            <div class="container_8 clearfix">                

                <!-- Main Section -->
                <section class="main-section grid_8">
                    <!-- Forms Section -->
                    <div class="main-content grid_5 alpha">
                        <header>
                            <h2>Registro de Clientes</h2>
                        </header>
                        <section class="clearfix">
                            <form class="form" action="controladores/cliente_controller.php?action=crear" method="POST">
                            <?php if (!empty($_GET['respuesta'])){ ?>
                                <?php if ($_GET['respuesta'] != "error") { ?>
                                <div class="message success closeable"><span class="message-close"></span>
                                    <h3>Correcto!</h3>
                                    <p>El Cliente se ha creado correctamente.</p>
                                </div>
                                <?php } else{ ?>
                                <div class="message error closeable"><span class="message-close"></span>
                                    <h3>Error!</h3>
                                    <p>El Cliente no se ha registrado correctamente.</p>
                                </div>
                                <?php } ?>
                            <?php } ?>
                                <div class="clearfix">
                                    <label>Tipo de Documento <em>*</em><small>Tipo de Documento</small></label>
                                    <select id="TipoDocumento" name="TipoDocumento">
                                    <option value="Cedula de Ciudadania">Cedula de Ciudadania</option>
                                    <option value="Cedula de Extrangeria">Cedula de Extrangeria</option>
                                    <option value="Targeta de Identidad">Targeta de Identidad</option>

                                </select>
                                </div>
                                <div class="clearfix">
                                    <label>Documento <em>*</em><small># Documento del Cliente</small></label><input type="text" name="Documento" id="Documento" required="Documento" maxlength="20" />
                                </div>
                                <div class="clearfix">
                                    <label>Nombres <em>*</em><small>Nombres Completos</small></label><input type="text" name="Nombres" id="Nombres" required="Nombres" maxlength="40" />
                                </div>
                                <div class="clearfix">
                                    <label>Apellidos <em>*</em><small>Apellidos Completos</small></label><input type="text" name="Apellidos" id="Apellidos" required="Apellidos" />
                                </div>
                                
                                <div class="clearfix">
                                    <label>Direcci&oacute;n <em>*</em><small>Lugar de Residencia</small></label><input type="text" name="Direccion" id="Direccion" required="Direccion" maxlength="25"/>
                                </div>

                                <div class="clearfix">
                                    <label>Telefono <em>*</em><small>Telefono del Cliente</small></label><input type="text" name="Telefono" id="Telefono" required="Telefono" maxlength="50" />
                                </div>

                                <div class="clearfix">
                                    <label>Estado <em>*</em><small>Estado del Cliente</small></label>
                                    <select id="Estado" name="Estado">
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                    </select>
                                </div>

                                <div class="clearfix">
                                    <label>Usuario <em>*</em><small>Ingrese su Usuario</small></label><input type="text" name="Usuario" id="Usuario" required="Usuario" />
                                </div>
                                <div class="clearfix">
                                    <label>Password <em>*</em><small>Ingrese su Password</small></label><input type="password" name="Contrasena" id="Contrasena" required="Contrasena" maxlength="20" />
                                </div>
                                                             
                                <div class="action clearfix">
                                    <button class="button button-gray" type="submit"><span class="accept"></span>OK</button>
                                    <button class="button button-gray" type="reset">Reset</button>
                                </div>
                            </form>
                        </section>
                    </div>
                    <!-- End Forms Section -->

                    <!-- Accordion Section -->
                    <div class="main-content grid_3 omega">
                        <header><h2>Instrucciones</h2></header>
                        <section class="accordion clearfix">
                            <header class="current"><h2>Datos del Usuario</h2></header>
                            <section style="display:block">
                                <h3>Nombres</h3>
                                <div class="shortcuts">
                    <ul class="clearfix">
                        <li><a href="#" title="Monitor Activities"><img src="images/woofunction-icons/activity_monitor.png" /><span>Activity</span></a></li>
                        <li><a href="#" title="Add/Edit Contacts"><img src="images/woofunction-icons/address_book_32.png" /><span>Contacts</span><em>300</em></a></li>
                        <li><a href="#" title="View Recent Comments"><img src="images/woofunction-icons/comment_32.png" /><span>Comments</span></a><em>20</em></li>
                        <li><a href="#" title="View Recent Orders"><img src="images/woofunction-icons/basket_32.png" /><span>Orders</span><em>20</em></a></li>
                        <li><a href="#" title="Read Mail"><img src="images/woofunction-icons/email_32.png" /><span>Mail</span><em>4</em></a></li>
                        <li><a href="#" title="Send Newsletters"><img src="images/woofunction-icons/newspaper_32.png" /><span>Newsletters</span></a></li>
                        <li><a href="#" title="View Alerts"><img src="images/woofunction-icons/error_button.png" /><span>Alerts</span><em>20</em></a></li>
                        <li><a href="#" title="View Reports"><img src="images/woofunction-icons/chart_32.png" /><span>Reports</span></a></li>
                        <li><a href="#" title="Advanced Search"><img src="images/woofunction-icons/search_button_32.png" /><span>Search</span></a></li>
                        <li><a href="#" title="Add/Edit Users"><img src="images/woofunction-icons/user_32.png" /><span>Users</span><em>5</em></a></li>
                    </ul>
                </div>
                <h3>Where does it come from?</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>
                            </section>
                        </section>
                    </div>
                    <!-- End Accordion Section -->

                    <div class="clear"></div>

                </section>

                <?php require_once("snippets/footer.php"); ?>
                <!-- Main Section End -->

            </div>
        </section>
    </div>

</body>
</html>
